# Skeletron Primer
## Tactics
Skeletron Prime must be fought at night.
It has 4 arms and a head.
* **cannon** shoots bombs up in the air (in rage mode, it shoots directly)
* **saw** goes under the player and moves up and down
* **vice** swipes around, often staying diagonal to player
* **laser** shoots at player, often staying horizontal

**Phase: "rage mode"**
Skeletron Prime's head spins, during which it has double attack and defense.
The arms attack more aggressively and quickly.
## Prep
### Debuffs
* Head: vulnerable to Ichor
* Arms: vulnerable to Ichor, Shadowflame, Cursed Inferno (but immune to regular fire!)
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTI3ODUyMjgyLC0yMDg4NzQ2NjEyLDczMD
k5ODExNl19
-->